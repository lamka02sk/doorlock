import sha from 'js-sha512'
import Storage from "./Storage";
import Crypto from './Helpers/Crypto'

export default {

  data() {
    return {
      password: '',
      passwordLogin: '',
      passwordBlurred: false,
      passwordValidation: {
        valid: true,
        text: ''
      },
      passwordAgain: '',
      passwordAgainBlurred: false,
      passwordAgainValidation: {
        valid: true,
        text: ''
      }
    }
  },

  watch: {

    password() {
      this.validatePassword();
      this.validatePasswordAgain();

    },

    passwordAgain() {
      this.validatePasswordAgain();
    }

  },

  methods: {

    validatePassword(blur = false) {

      const length = this.password.length;

      if (length > 256) {

        this.passwordValidation = {
          valid: true,
          text: 'Oh, you freak!!!'
        };

        this.passwordBlurred = true;

      } else if(length > 11) {

        this.passwordValidation = {
          valid: true,
          text: 'Good to go!'
        };

        this.passwordBlurred = true;

      } else
        this.passwordValidation = {
          valid: true,
          text: ''
        };

      if(blur)
        this.passwordBlurred = true;

      if(!blur && !this.passwordBlurred)
        return this.passwordValidation.valid;

      if(length < 1 && this.passwordBlurred)
        this.passwordValidation = {
          valid: false,
          text: 'They won\'t crack your password if you don\'t have any, right?'
        };
      else if(length < 12 && this.passwordBlurred)
        this.passwordValidation = {
          valid: false,
          text: 'Password should contain at least 12 characters'
        };

      return this.passwordValidation.valid;

    },

    validatePasswordAgain() {

      if(this.password === this.passwordAgain) {
        this.passwordAgainValidation = {
          valid: true,
          text: 'Nice'
        };
      }

      if(this.password !== this.passwordAgain)
        this.passwordAgainValidation = {
          valid: false,
          text: 'Passwords does not match'
        };

      return this.passwordAgainValidation.valid;

    },

    generateRandomNumber(min = 0, max = 999999) {
      return Math.floor(Math.random() * Math.floor(max - min)) + min;
    },

    generateRandomString(number) {
      return sha.sha512(number.toString());
    },

    generateTestString() {

      const length = this.generateRandomNumber(256, 512);
      let string = '';

      while(string.length < length)
        string += this.generateRandomString(this.generateRandomNumber());

      return string.substring(0, length);

    },

    setup() {

      if(!this.validatePassword(true) || !this.validatePasswordAgain())
        return true;

      // INPUT:
      // test string (hidden, random, random length)
      // password
      // password again
      // THEN:
      // hash test string with sha-512
      // encrypt test string with password and save
      // save hash of test string
      // AUTH:
      // decrypt test string
      // hash decrypted string with sha-512
      // compare hashes

      const testString = this.generateTestString();
      const testStringHash = sha.sha512(testString);
      const testStringEncrypted = Crypto.encrypt(this.password, testString);
      this.$store.commit('setSecret', this.password);

      Storage.query('main').add({
        testHash: testStringHash,
        testEncrypted: testStringEncrypted
      }).then(() => {
        this.$store.commit('setFirstSetup', false);
        this.$store.commit('finishSetup');
      }).catch(() => {
        this.$store.commit('setSetupError');
      });

    },

    auth() {

      if(this.passwordLogin.length < 12)
        return this.$store.commit('setSetupError');

      const testStringDecrypted = Crypto.decrypt(this.passwordLogin, this.$store.state.auth.testEncrypted);
      const testStringHash = sha.sha512(testStringDecrypted);

      if(testStringHash !== this.$store.state.auth.testHash)
        return this.$store.commit('setSetupError');

      this.$store.commit('setSecret', this.passwordLogin);
      this.$store.commit('finishSetup');

    }

  }

}
