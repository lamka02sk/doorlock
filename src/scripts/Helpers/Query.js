export default class {

  constructor(transaction) {
    this.transaction = transaction;
    this.request = null;
  }

  get(key) {
    return new Promise((resolve, reject) => {
      this.request = this.transaction.get(key);
      this.request.onsuccess = result => resolve(result.target.result);
      this.request.onerror = () => reject(undefined);
    });
  }

  getAll() {
    return new Promise((resolve, reject) => {
      this.request = this.transaction.getAll();
      this.request.onsuccess = result => resolve(result.target.result);
      this.request.onerror = () => reject(undefined);
    });
  }

  add(value) {
    return new Promise((resolve, reject) => {
      this.request = this.transaction.add(value);
      this.request.onsuccess = () => resolve(value);
      this.request.onerror = () => reject();
    });
  }

}
