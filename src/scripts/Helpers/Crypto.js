const aes = require('aes-js');
const pbkdf2 = require('pbkdf2');

export default {

  deriveKey(key) {
    return pbkdf2.pbkdf2Sync(key, '', 1000, 32, 'sha512');
  },

  encrypt(key, data) {

    key = this.deriveKey(key);
    const textBytes = aes.utils.utf8.toBytes(data);

    const instance = new aes.ModeOfOperation.ctr(key);
    const encryptedBytes = instance.encrypt(textBytes);

    return aes.utils.hex.fromBytes(encryptedBytes);

  },

  decrypt(key, data) {

    key = this.deriveKey(key);
    const textBytes = aes.utils.hex.toBytes(data);

    const instance = new aes.ModeOfOperation.ctr(key);
    const decryptedBytes = instance.decrypt(textBytes);

    return aes.utils.utf8.fromBytes(decryptedBytes);

  }

}
