import Vue from 'vue'

export default new Vue({
  install() {
    Vue.prototype.$bus = this;
  }
});
