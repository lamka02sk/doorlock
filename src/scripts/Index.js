import Storage from './Storage'
import Modals from './../components/Modal.vue'
import Setup from './../components/Setup.vue'

export default {

  components: { Modals, Setup },

  data() {
    return {
      modal: {},
    }
  },

  created() {

    this.modal = Storage.checkSupport();
    Storage.prepare();

    Storage.connection.onsuccess = result => {
      Storage.db = result.target.result;
      this.firstSetup();
    };

  },

  methods: {

    hideModal() {
      this.modal = null;
    },

    firstSetup() {

      Storage.query('main').getAll().then(result => {

        if(result.length > 0)
          return this.loadData(result[0]);

        this.$store.commit('setFirstSetup');

      });

    },

    loadData(data = null) {

      if(data)
        return this.$store.commit('setAuth', data);

      Storage.query('main').getAll().then(result => {

        if(result.length < 1) {
          document.body.innerHTML = 'There was an unrecoverable error';
          return true;
        }

        this.$store.commit('setAuth', result[0]);

      }).catch(() => {
        document.body.innerHTML = 'There was an unrecoverable error';
      })

    }

  },

}
