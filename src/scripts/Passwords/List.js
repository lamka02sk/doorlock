import Item from '../../components/Passwords/Item'
import Storage from "../Storage";

export default {

  components: {
    'passwords-list-item': Item
  },

  data() {
    return {
      passwords: []
    }
  },

  created() {

    // update data

  },

  watch: {
    "$store.state.secret"() {

      const secret = this.$store.state.secret;
      if(!secret)
        return true;

      this.loadPasswords(secret);

    }
  },

  methods: {

    loadPasswords(secret) {

      /*Storage.query('passwords').getAll().then(result => {



      }).catch(() => {

      });*/

    }

  }

}
