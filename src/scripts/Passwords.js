import List from '../components/Passwords/List'
import New from '../components/Passwords/New'

export default {

  components: {
    'passwords-list': List,
    'passwords-new': New,
    'search': ''
  },

  data() {
    return {
      formOpened: false,
    }
  },

  methods: {

    toggleForm() {
      this.formOpened = !this.formOpened;
    }

  }

}
