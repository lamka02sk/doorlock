import Query from "./Helpers/Query";

const Storage = {

  dbVersion: 1,
  connection: null,
  indexedDB: null,

  checkSupport() {

    // Check for IndexedDB support
    this.fullBrowserSupport = !!window.indexedDB;
    this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    this.browserSupport = !!window.indexedDB;
    this.db = null;

    if(!this.browserSupport)
      return {
        title: 'Unsupported browser!',
        close: false,
        content: 'You are using unsupported browser. In order to use doorlock, you will need to update your browser.'
      };
    else if(this.browserSupport && !this.fullBrowserSupport)
      return {
        title: 'Your browser is deprecated!',
        close: true,
        content: 'Even though your current browser should support doorlock, you may experience errors. We really ' +
          'suggest you to update your web browser.'
      };

    return null;

  },

  prepare() {

    this.connection = this.indexedDB.open('doorlockStorage', this.dbVersion);

    this.connection.onupgradeneeded = result => {

      this.db = result.target.result;

      Object.keys(this.migrations).forEach(migration => {
        if(migration <= result.newVersion && migration > result.oldVersion)
          this.migrations[migration](this.db);
      });

    };

  },

  query(store) {
    try {
      const transaction = this.db.transaction(store, 'readwrite').objectStore(store);
      return new Query(transaction);
    } catch(e) {
      return null;
    }
  },

  migrations: {

    1(db) {

      const mainStore = db.createObjectStore('main', {
        keyPath: 'id',
        autoIncrement: true
      });

      mainStore.createIndex('testHash', 'testHash', { unique: true });
      mainStore.createIndex('testEncrypted', 'testEncrypted', { unique: true });

      const passwordsStore = db.createObjectStore('passwords', {
        keyPath: 'id',
        autoIncrement: true
      });

      passwordsStore.createIndex('title', 'title', { unique: true });
      passwordsStore.createIndex('note', 'note', { unique: false });
      passwordsStore.createIndex('service', 'service', { unique: false });
      passwordsStore.createIndex('password', 'password', { unique: false });
      passwordsStore.createIndex('created', 'created', { unique: false });
      passwordsStore.createIndex('updated', 'updated', { unique: false });

    }

  }

};

export default Storage;
