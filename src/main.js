import Vue from 'vue'
import Vuex from 'vuex'
import Index from './components/Index'
import router from './router'
import './styles/Main.sass'

import EventBus from './scripts/Helpers/Bus'

Vue.config.productionTip = true;
Vue.use(Vuex);
Vue.use(EventBus);

const store = require('./store').default;

new Vue({
  el: '#app',
  router,
  store,
  template: '<Index/>',
  components: { Index },

  mounted() {
      this.splash();
  },

  methods: {

    splash() {

      const splashScreen = document.querySelector('.splash-screen');
      const splashName = splashScreen.querySelector('.name');
      const splashLoader = splashScreen.querySelector('.loader');

      splashName.classList.add('animate');
      splashLoader.classList.add('animate');
      splashScreen.classList.add('animate');

    }

  }
});
