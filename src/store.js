import Vuex from "vuex";

export default new Vuex.Store({

  state: {
    setup: {
      done: false,
      first: false,
      error: false,
    },
    secret: null,
    auth: null
  },

  mutations: {
    setFirstSetup(state, value = true) {
      state.setup.first = value;
    },
    finishSetup(state) {
      state.setup.done = true;
    },
    setSecret(state, value) {
      state.secret = value;
    },
    setSetupError(state) {
      state.setup.error = true;
    },
    setAuth(state, data) {
      state.auth = data;
    }
  }

});
